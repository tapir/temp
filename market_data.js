var market_data = {}

function get_new_data() {
    var api_currencies = fetch("https://api.atalante.co/v1/currencies").then(response => {
        if (!response.ok) { throw response }
        return response.json()
    })
    var api_currency_pairs = fetch("https://api.atalante.co/v1/currency_pairs").then(response => {
        if (!response.ok) { throw response }
        return response.json()
    })
    var api_limits = fetch("https://api.atalante.co/v1/limits").then(response => {
        if (!response.ok) { throw response }
        return response.json()
    })
    var api_prices = fetch("https://api.atalante.co/v1/prices").then(response => {
        if (!response.ok) { throw response }
        return response.json()
    })

    Promise.all([
        api_currencies,
        api_currency_pairs,
        api_limits,
        api_prices
    ]).then(function (values) {
        var new_data = {}
        new_data.coins = []
        new_data.coin_filter = {}
        new_data.coin_pair = {}
        new_data.limits = {}
        new_data.prices = {}

        // Coins
        for (var i = 0; i < values[0].length; i++) {
            new_data.coins.push(values[0][i].symbol)
        }

        for (var i = 0; i < values[1].length; i++) {
            var base = values[1][i].base_currency.symbol
            var quote = values[1][i].quote_currency.symbol
            var symbol = values[1][i].symbol

            // Coin -> Coins Filtered
            if (!(base in new_data.coin_filter)) {
                new_data.coin_filter[base] = [quote]
            } else {
                if (!new_data.coin_filter[base].includes(quote)) {
                    new_data.coin_filter[base].push(quote)
                }
            }
            if (!(quote in new_data.coin_filter)) {
                new_data.coin_filter[quote] = [base]
            } else {
                if (!new_data.coin_filter[quote].includes(base)) {
                    new_data.coin_filter[quote].push(base)
                }
            }

            // Coin + Coin -> Pair + Side
            new_data.coin_pair[base + "_" + quote] = {
                symbol: symbol,
                side: "sell"
            }
            new_data.coin_pair[quote + "_" + base] = {
                symbol: symbol,
                side: "buy"
            }
        }

        // Limits
        for (var i = 0; i < values[2].length; i++) {
            new_data.limits[values[2][i].currency.symbol] = {
                min: values[2][i].limit_min,
                max: values[2][i].limit_max,
            }
        }

        // Prices
        for (var i = 0; i < values[3].length; i++) {
            var name = (values[3][i].currency_pair.base_currency.symbol + "/" +
                values[3][i].currency_pair.quote_currency.symbol).toUpperCase()
            new_data.prices[name] = values[3][i].price
        }

        market_data = new_data
        console.log("Market data updated")
    }).catch(function (errCode) {
        console.log("Can't get data from api.atalante.co: " + errCode)
    })
}
